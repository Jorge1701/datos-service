const mongoose = require('mongoose');

const categoriaSchema = mongoose.Schema({
    nombre: {
        type: String,
        unique: true,
        required: true
    }
});

mongoose.model('Categoria', categoriaSchema);
