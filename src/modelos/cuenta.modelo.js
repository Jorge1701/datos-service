const mongoose = require('mongoose');

const cuentaSchema = mongoose.Schema({
    nombre: {
        type: String,
        unique: true,
        required: true
    },
    saldo: {
        type: Number,
        required: true
    }
});

mongoose.model('Cuenta', cuentaSchema);
