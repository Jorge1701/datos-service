const mongoose = require('mongoose');

const movimientoSchema = mongoose.Schema({
    cuenta: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Cuenta',
        required: true
    },
    tipo: {
        type: String,
        required: true
    },
    cantidad: {
        type: Number,
        required: true
    },
    categoria: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Categoria',
        required: true
    },
    nota: {
        type: String
    },
    fecha: {
        type: Date,
        required: true
    }
});

mongoose.model('Movimiento', movimientoSchema);
