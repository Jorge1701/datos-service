require('./mongoose/mongoose.configurar');
const express = require('express');
const cors = require('cors');
const bodyParser = require('body-parser');
const PORT = process.env.PORT || 3000;

const app = express();
app.use(cors());
app.use(bodyParser.json());

app.use('/cuentas', require('./controladores/cuentas.controller'));
app.use('/categorias', require('./controladores/categorias.controller'));
app.use('/movimientos', require('./controladores/movimientos.controller'));

app.listen(PORT, () => {
    console.log('Servidor iniciado en puerto: ', PORT);
});
