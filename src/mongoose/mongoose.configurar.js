require('../modelos/cuenta.modelo');
require('../modelos/categoria.modelo');
require('../modelos/movimiento.modelo');

const mongoose = require('mongoose');
const config = require( '../../config' );

mongoose.connect(config.mongoUri, {
    useNewUrlParser: true,
    useCreateIndex: true,
    useUnifiedTopology: true,
    useFindAndModify: false
});

mongoose.connection.on('connected', () => {
    console.log('Conectado la instancia de mongo');
});

mongoose.connection.on('error', error => {
    console.log('Error conectando a mongo', error);
});
