const mongoose = require('mongoose');
const Movimiento = mongoose.model('Movimiento');

const router = require('express').Router();

router.get('', async (req, res) => {
    const id = req.query.id;

    if (!id) {
        res.send(await Movimiento.find().sort('-fecha').populate('cuenta').populate('categoria'));
    } else {
        res.send(await Movimiento.findById(id).populate('cuenta').populate('categoria'));
    }
});

router.post('', async (req, res) => {
    const movimiento = req.body;

    if (!movimiento) {
        return res.status(400).send('Falta objeto movimiento');
    } else if (!movimiento.cuenta) {
        return res.status(400).send('Falta cuenta');
    } else if (!movimiento.tipo) {
        return res.status(400).send('Falta tipo');
    } else if (!movimiento.cantidad) {
        return res.status(400).send('Falta cantidad');
    } else if (!movimiento.categoria) {
        return res.status(400).send('Falta categoria');
    } else if (!movimiento.fecha) {
        return res.status(400).send('Falta fecha');
    }

    res.send(await new Movimiento(movimiento).save());
});

router.patch('', async (req, res) => {
    res.sendStatus(200);
});

router.delete('', async (req, res) => {
    const id = req.query.id;

    if (!id || id.trim() === '') {
        return res.status(400).send('Falta id');
    }

    res.send(await Movimiento.findByIdAndDelete(id));
});

module.exports = router;
