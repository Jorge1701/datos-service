const mongoose = require('mongoose');
const Cuenta = mongoose.model('Cuenta');

const router = require('express').Router();

router.get('', async (req, res) => {
    const nombre = req.query.nombre;

    if (!nombre) {
        res.send(await Cuenta.find());
    } else {
        res.send(await Cuenta.findOne({ nombre }));
    }
});

router.post('', async (req, res) => {
    const cuenta = req.body;

    if (await validarCuenta(cuenta, res)) {
        if (await Cuenta.exists({ nombre: cuenta.nombre })) {
            return res.status(409).send('Cuenta ya existe');
        }

        res.send(await new Cuenta(cuenta).save());
    }
});

router.patch('', async (req, res) => {
    const cuenta = req.body;

    if (await validarCuenta(cuenta, res)) {
        const mismoNombre = await Cuenta.findOne({ nombre: cuenta.nombre });

        if (mismoNombre && String(mismoNombre._id) !== String(cuenta._id)) {
            return res.status(409).send('Nombre ya existe');
        }

        await Cuenta.findByIdAndUpdate(cuenta._id, cuenta);
        res.send(await Cuenta.findById(cuenta._id));
    }
});

router.delete('', async (req, res) => {
    const id = req.query.id;

    if (!id || id.trim() === '') {
        return res.status(400).send('Falta id');
    }

    res.send(await Cuenta.findByIdAndDelete(id));
});

async function validarCuenta(cuenta, res) {
    if (!cuenta) {
        res.status(400).send('Falta objeto cuenta');
        return false;
    } else if (!cuenta.nombre || cuenta.nombre.trim() === '') {
        res.status(400).send('Falta nombre');
        return false;
    } else if (!cuenta.saldo) {
        res.status(400).send('Falta saldo');
        return false;
    }
    return true;
}

module.exports = router;
