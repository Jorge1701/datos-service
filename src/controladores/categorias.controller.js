const mongoose = require('mongoose');
const Categoria = mongoose.model('Categoria');

const router = require('express').Router();

router.get('', async (req, res) => {
    const nombre = req.query.nombre;

    if (!nombre) {
        res.send(await Categoria.find());
    } else {
        res.send(await Categoria.findOne({ nombre }));
    }
});

router.post('', async (req, res) => {
    const nombre = req.body.nombre;

    if (!nombre || nombre.trim() === '') {
        return res.status(400).send('Falta nombre');
    }

    if (await Categoria.exists({ nombre })) {
        return res.status(409).send('Categoria ya existe');
    }

    res.send(await new Categoria({ nombre }).save());
});

router.patch('/nombre', async (req, res) => {
    const id = req.body.id;
    const nombre = req.body.nombre;

    if (!id || id.trim() === '') {
        return res.status(400).send('Falta id');
    } else if (!nombre || nombre.trim() === '') {
        return res.status(400).send('Falta nombre');
    }

    if (await Categoria.exists({ nombre })) {
        return res.status(409).send('Categoria ya existe');
    }

    await Categoria.findByIdAndUpdate(id, { nombre });
    res.send(await Categoria.findById(id));
});

router.delete('', async (req, res) => {
    const id = req.query.id;

    if (!id || id.trim() === '') {
        return res.status(400).send('Falta id');
    }

    res.send(await Categoria.findByIdAndDelete(id));
});

module.exports = router;
